#!/usr/bin/perl

# FaSilET²
# Copyright (C) 2019 Silicom
#
# Author : Cédric PELLERIN (cpellerin@silicom.fr)
# Latest version might be found at http://github.com/fasilet
#
# This file is part of FaSilET² project
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

use strict;
use warnings;

use lib '/home/cedric/Developpement/Silicom';
use ET2::Validators::SSHLogs;

use Data::Dumper;

sub run
{
	my $debug = 1;
	my $ret;
	my $host = "10.10.2.4";

	my $vlog = new ET2::Validators::SSHLogs(
		host => $host,
		port => 10022,
		key => '/home/cedric/Developpement/LacroixSofrel/SG4K-Sofrel/project-A3/keys/ssh.key',
		logfile => '/var/log/SG4000/diag_en.log',
		timeout => '0..20',
		autostart => 1,
		user => "root"
	);

	return undef if(!defined($vlog));
	$vlog->setregexok("WARN admin_vpn0: connection");
	$vlog->setname("Log1");
	$vlog->setdebug($debug);
	$vlog->tail();
	return $vlog->getresults();
}

run();
