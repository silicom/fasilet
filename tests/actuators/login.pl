#!/usr/bin/perl

# FaSilET²
# Copyright (C) 2019 Silicom
#
# Author : Cédric PELLERIN (cpellerin@silicom.fr)
# Latest version might be found at http://github.com/fasilet
#
# This file is part of FaSilET² project
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

use strict;
use warnings;
use lib '/home/cedric/Developpement/Silicom';
use 5.20.0;

use File::Basename;
my $dirname = dirname(__FILE__);
require "$dirname/share.pl";
# Shared variables also defined in share.pl to use with other actuators
our $report;
our $sel;

use ET2::Web::Selenium;
use ET2::Utils::Report;

our @VALIDATORS = ( 'Log1' );

my $debug = 1;
my $host = "10.10.2.4";

if(!defined($sel))
{
	$sel = ET2::Web::Selenium->new(
		host => "https://$host",
		driver => "firefox",
		encoding => 'latin1',
	);

	$sel->setname("Sel1");
	$sel->setdebug($debug);
	$sel->start();
}

if(!defined($report))
{
	$report = ET2::Utils::Report->new(
		title => 'SG4000 tests',
		description => 'End to end tests for SG4000 project'
	);
}

my $ret;
my $txt;
my $retip;
my $rettitle;
my $returl;

say "************ TEST : Good login *********";

my $testname = 'goodlogin';
my $testdesc = "wait_for_ipconfig";

$sel->goto('/');
$sel->write(name => "username", value => "admin_vpn0");
$sel->write(name => "password", value => "admin_vpn1");
$sel->clickbtn(class_name => "btn");

$ret = $sel->waitforpage(
	timeout => 2,
	url => 'https://10.10.2.4/#/configuration/localnetwork/ipconfig'
);

if($ret > 0)
{
	say "Timeout waiting for ipconfig page";
	say "Test FAILED";
	$report->addresultdesc($testname, $testdesc, 'Waiting for ipconfig page');
	$report->addresult($testname, $testdesc, 'FAILED');
}
else
{
	$rettitle = $sel->gettitle();
	$returl = $sel->geturl();

	if(($returl =~ /config$/) && ($rettitle =~ /^Param/))
	{
		say "Good login : TEST SUCCESSFUL";
		$retip = $sel->read(name => "ip");
		say "IP = $retip";
		$report->addresult($testname, $testdesc, 'SUCCESS');
		$report->addresultdesc($testname, $testdesc, 'Check page name and url');
		$report->addresultcomment($testname, $testdesc, "IP read : $retip");
	}
	else
	{
		say "Good login : TEST FAILED";
		$report->addresult($testname, $testdesc, 'FAILED');
		$report->addresultdesc($testname, $testdesc, 'Check page name and url');
	}
}
say "************* TEST terminated ***************";

