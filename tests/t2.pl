#!/usr/bin/perl -w

# FaSilET²
# Copyright (C) 2019 Silicom
#
# Author : Cédric PELLERIN (cpellerin@silicom.fr)
# Latest version might be found at http://github.com/fasilet
#
# This file is part of FaSilET² project
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

use strict;
use lib '/home/cedric/Developpement/Silicom';

use ET2::Selenium;

use Data::Dumper;

my $debug = 1;
my $ret;
my $host = "10.10.2.4";
my $sel = ET2::Selenium->new(
	host => "https://$host",
	parallel => 20,					# Launch 20 times in containers
	# loop => 100,					# Loop 100 times
	validators => [
		'Log1',
		'Log2'
	]
);

$sel->setname("Sel1");
$sel->setdebug($debug);
$sel->goto('/');
$sel->write(name => "username", value => "admin_vpn0");
$sel->write(name => "password", value => "admin_vpn1");
$sel->clickbtn(class_name => "btn");
sleep 1;
$ret = $sel->gettitle();
print "Title : $ret\n";
$ret = $sel->geturl();
print "URL : $ret\n";
$ret = $sel->read(name => "ip");
print "IP = $ret\n";

#print Dumper($ret);
