#!/usr/bin/perl

# FaSilET²
# Copyright (C) 2019 Silicom
#
# Author : Cédric PELLERIN (cpellerin@silicom.fr)
# Latest version might be found at http://github.com/fasilet
#
# This file is part of FaSilET² project
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

use strict;
use warnings;

use lib '/home/cedric/Developpement/Silicom/End2End';
use ET2::Utils::LXC;
use ET2::Utils::Report;
use ET2::Utils::IniParser;

use JSON;
use HTTP::Async;
use HTTP::Request;

use Data::Dumper;

our $modpath = '/home/cedric/Developpement/Silicom/End2End';
our $dir = $ARGV[0] or die("Usage : $0 <base_directory>");
our %container;
our %actuators;
our $results;

our $DEBUG = 1;

sub debug
{
	my $str = shift;

	print "$str\n" if($DEBUG);
}

# Create base container for validators
#
sub basecreate
{
	my $c;
	my $ret;

	debug("Creating BaseValidator container... ");
	$c = ET2::Utils::LXC->new(
		name => "BaseValidators",
		template => 'debian/sid/amd64'
	);

	$ret = $c->create();
	if($ret > 0)
	{
		$ret = $c->error();
		debug("$ret");
	}

	$ret = $c->start();
	if($ret > 0)
	{
		$ret = $c->error();
		debug("$ret");
	}

	sleep 10;
	$ret = $c->execute("apt-get update");
	$ret = $c->execute("apt-get install -y perl libnet-http-perl libnet-httpserver-perl libjson-perl libnetaddr-ip-perl libipc-system-simple-perl libnet-openssh-perl");
	$ret = $c->push("$modpath/ET2", "/usr/share/perl5");
	$ret = $c->execute("chmod +x /usr/share/perl5/ET2/Validators/httpserver.pl");
	debug("Done");
}

# Instanciate every validators containers inheriting from BaseValidators
# IC : List of validator names
# OC : N/A
#
sub instanciate
{
	my @v = @_;
	my $i;

	my $fh;
	my $fhclone;
	my $c;
	my $ret;
	my $key;
	my $auto;
	my $json;
	my $line;

	for $i (@v)
	{
		$key = "";
		$line = "";
		open($fh, "<", "$dir/validators/$i.pl");
		open($fhclone, ">", "/tmp/$i.pl");
		while($line = <$fh>)
		{
			if($line =~ /new\s+ET2::Validators::/)
			{
				print $fhclone $line;
				while($line = <$fh>)
				{
					if($line =~ /key\s*=>\s*(.*?)$/)
					{
						$key = $1;
						$key =~ s/,\s*$//;
						$key =~ s/\'\s*//g;
						$line =~ s/\/.*\//\/root\//;
					}
					if($line =~ /autostart\s*=>\s*(.*?)$/)
					{
						$auto = $1;
						$auto =~ s/,\s*$//;
					}
					last if($line =~ /\);/);
					print $fhclone $line;
				}
			}
			print $fhclone $line;
		}
		close($fhclone);
		close($fh);

		$container{$i}->{'key'} = $key;
		$container{$i}->{'auto'} = $auto;

		debug("$i container : Creation... ");
		$c = ET2::Utils::LXC->new(
			name => "$i",
			heritate => 'BaseValidators'
		);

		$ret = $c->create();
		if($ret == 1) 
		{
			basecreate();
			$ret = $c->create();
		}
		if($ret > 0)
		{
			$ret = $c->error();
			debug("$ret");
			exit 1;
		}

		debug("Start... ");
		$ret = $c->start();
		if($ret > 0)
		{
			$ret = $c->error();
			debug("$ret");
			exit 1;
		}
		$ret = $c->push($key, "/root") if($key ne "");
		$ret = $c->push("/tmp/$i.pl", "/root");
		$container{$i}->{'ip'} = $c->getip();
		$ret = $c->execute("start-stop-daemon --start -b --exec /usr/share/perl5/ET2/Validators/httpserver.pl");
		debug("Done");
	}
}


sub createvalidators
{
	my $filename;
	my $dh;
	my $fh;
	my @valid;
	my $line;
	my $instr;
	my $ini;
	my $str;

	# Old style, reading actuators list in directory and sorting by name. Kept just in case...
	#	opendir($dh, "$dir/actuators") || die "Can't opendir $dir: $!";
	#	@act = sort { lc($a) cmp lc($b) } grep { $_ !~ /^\./ && $_ =~ /pl$/ } readdir($dh);
	#	closedir $dh;

	die("Can't find et2.conf in $dir") if (! -f "$dir/et2.conf");
	$ini = ET2::Utils::IniParser->new();
	$ini->parse("$dir/et2.conf");

	$str = $ini->getitem("header", "title");
	$results = ET2::Utils::Report->new(
		title => $str
	);
	$str = $ini->getitem("header", "description");
	$results->setdescription($str);

	foreach my $k ($ini->getkeys("actuators"))
	{
		$results->addactdesc($k, $ini->getitem("actuators", $k));
	}

	for $filename ($ini->getkeys("actuators"))
	{
		open($fh, "<", "$dir/actuators/$filename.pl") or next;
		@valid = ();
		while($line = <$fh>)
		{
			chomp($line);
			if($line =~ /\@VALIDATORS\s*=/)
			{
				$instr = $line;
				while($line !~ /\)/)
				{
					$line = <$fh>;
					chomp($line);
					$instr .= $line;
				}
				$instr =~ s/our \@VALIDATORS/\@valid/;
				eval $instr;
			}
		}
		close($fh);
		instanciate(@valid);
		# Add validators list to actuators hash
		push(@{$actuators{$filename}->{'validators'}}, @valid);
	}
	# Store sorted list of actuators for execution
	push(@{$actuators{'list'}}, $ini->getkeys("actuators"));
}

sub execute
{
	my $act = shift;
	my $valid;
	my $ret;
	my $async;
	my $req;
	my $url;
	my $answer;
	my $pid;
	my $header;
	my $data;
	my $val;
	my $status;

	$async = HTTP::Async->new();
	for $valid (@{$actuators{$act}->{'validators'}})
	{
		if($container{$valid}->{'auto'} == 1)
		{
			$header = ['Content-Type' => 'application/json; charset=UTF-8'];
			$data = "{\"name\":\"$valid.pl\"}";
			$url = "http://$container{$valid}->{'ip'}:9000/start";
			$req = HTTP::Request->new('POST', $url, $header, $data);
			$async->add($req);
		}
	}

	debug("Executing $dir/actuators/$act.pl... ");
	$ret = do("$dir/actuators/$act.pl");
	debug("Done.");
	$async->poke;

	debug("Waiting for http server answer");
	while($answer = $async->wait_for_next_response)
	{
		$async->poke;
		if($answer->{'_content'} =~ /{\"status\"\:\"(\w+)\"}/)
		{
			debug("Got answer");
			$status = $1;
			$val = $answer->{'_request'}->{'_content'};
			$val = $val =~ /{\"name\"\:\"(\w+)\.pl\"}/ ? $1 : 'unknown';
			$results->addvalstatus($act, $val, $status);
		}
	}
	return 0;
}

sub getresults
{
	my $act = shift;
	my $valid;
	my $header;
	my $data;
	my $url;
	my $req;
	my $async;
	my $response;
	my $json;
	
	$async = HTTP::Async->new();
	for $valid (@{$actuators{$act}->{'validators'}})
	{
		$header = ['Content-Type' => 'application/json; charset=UTF-8'];
		$data = "{\"name\":\"$valid.pl\"}";
		$url = "http://$container{$valid}->{'ip'}:9000/results";
		$req = HTTP::Request->new('POST', $url, $header, $data);
		$async->add($req);
		while($response = $async->wait_for_next_response )
		{
			if(defined($response->{'_content'}))
			{
				eval
				{
					$json = decode_json($response->{'_content'});
				};
				$json = '' if($@);
				$results->addvalresults($act, $valid, $json);
			}
		}
	}
}

createvalidators();
debug("Ready");

for my $act (@{$actuators{'list'}})
{
	execute($act);
	getresults($act);
}
$results->export("/tmp/ET2results.json");

debug("TERMINATED");
#$SIG{"USR1"}=\&execute;
#while(1){}
