package ET2::Utils::LXC;

# FaSilET²
# Copyright (C) 2019 Silicom
#
# Author : Cédric PELLERIN (cpellerin@silicom.fr)
# Latest version might be found at http://github.com/fasilet
#
# This file is part of FaSilET² project
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
use strict;
use warnings;
 
use Exporter qw(import);
	 
sub new
{
	my $class = shift;
	my (%params) = @_;

	my $self = {};

	bless($self, $class);
	$self->{'name'} = $params{'name'} || $self->_randname();
	$self->{'template'} = $params{'template'} || "";
	$self->{'backingstore'} = $params{'backingstore'} || "dir"; # Can be dir', 'lvm', 'loop', 'btrfs', 'zfs', 'rbd'
	$self->{'netif'} = $params{'netif'} || "eth0";
	$self->{'ipaddr'} = $params{'ipaddr'} || "dhcp";
	$self->{'ipmask'} = $params{'ipmask'} || "";
	$self->{'gw'} = $params{'gw'} || "";
	$self->{'bridge'} = $params{'bridge'} || "lxcbr0";
	$self->{'macaddr'} = $params{'macaddr'} || "";
	$self->{'heritate'} = $params{'heritate'} || "";

	return $self;
}

sub create
{
	my $self = shift;

	my $ret;
	
	$self->stop() if($self->_isrunning());
	$self->destroy() if($self->_iscreated());
	if($self->{'template'} =~ /^(?<tn>\w+)\/(?<tv>\w+)\/(?<ta>\w+)$/)
	{
		$ret = `lxc-create -n $self->{'name'} -l DEBUG -o /tmp/lxc.log -t download -- -d $+{'tn'} -r $+{'tv'} -a $+{'ta'} 2>&1`;
		if($? != 0)
		{
			$self->_seterror($ret);
			return(1);
		}
		$self->_netconfig();
		return(0);
	}
	if($self->{'heritate'} =~ /^\w+$/)
	{
		if(! $self->_iscreated($self->{'heritate'}))
		{
			$self->_seterror("Parent container $self->{'heritate'} doesn't exist");
			return(1);
		}
		$self->stop($self->{'heritate'}) if($self->_isrunning($self->{'heritate'}));
		$ret = `lxc-copy -n $self->{'heritate'} -N $self->{'name'} -B overlayfs -s -o- -l ERROR 2>&1`;
		if($? != 0)
		{
			$self->_seterror($ret);
			return(1);
		}
		$self->_netconfig();
		return(0);
	}		
	$self->_seterror("Template format must be distribution/release/arch or use heritage");
	return(2);
}

sub error
{
	my $self = shift;

	return($self->{'error'});
}

sub start
{
	my $self = shift;
	my $ret;

	$ret = `lxc-start -n $self->{'name'} -d`;
	if($? > 0)
	{
		$self->_seterror($ret);
		return(1);
	}
	return(0);
}

sub stop
{
	my $self = shift;
	my $name = shift;
	my $ret;
	
	$name = $self->{'name'} if(!defined($name));
	$ret = `lxc-stop -n $name 2>&1`;
	if($? > 0)
	{
		$self->_seterror($ret);
		return(1);
	}
	return(0);
}

sub destroy
{
	my $self = shift;
	my $ret;

	$self->stop() if($self->_isrunning());
	$ret = `lxc-destroy -n $self->{'name'} 2>&1`;
	if($? > 0)
	{
		$self->_seterror($ret);
		return(1);
	}
	return(0);
}

sub push
{
	my $self = shift;
	my $from = shift;
	my $to = shift;
	my $dir;

	if (! -r $from)
	{
		$self->_seterror("Source directory or file not readable");
		return(1);
	}

	$dir = $self->_findlxcroot();
	return(1) if($dir eq '');

	$dir .= "/$to";
	if (! -d $dir)
	{
		if(system("mkdir -p $dir") > 0)
		{
			$self->_seterror("Can't create $dir");
			return(1);
		}
	}
	if (! -w $dir)
	{
		$self->_seterror("Destination directory not writable");
		return(1);
	}

	if( -d $from)
	{
		system("cp -r $from $dir");
	}
	else
	{
		system("cp $from $dir");
	}
	return(0);
}

sub setconfig
{
	my $self = shift;
	my (%params) = @_;
	my $conf;
	my $fh;
	my %ini = ();

	$conf = $self->_findlxcconfig();
	# If config file doesn't exist, create it with a little title
	if( ! -f "$conf")
	{
		open($fh, ">", "$conf");
		if(!defined($fh))
		{
			$self->seterror("Can't create configuration file $conf cause $!");
			return(1);
		}
		print $fh "# Configuration file for $self->{'name'} container\n";
		close($fh);
	}
	# Get in %ini hash current values from the config file
	open($fh, "<", "$conf");
	%ini = map { chomp; split /\s*=\s*/ } grep { $_ !~ /^#/ } <$fh>;
	close($fh);
	# Merge with values in %params
	%ini = (%ini, %params);
	# Write the new config file
	open($fh, ">", "$conf");
	print $fh map { "$_ = $ini{$_}\n"; } keys(%ini);
	close($fh);
}

sub execute
{
	my $self = shift;
	my $cmd = shift;

	my $ret;

	$ret = `lxc-attach -n $self->{'name'} -- $cmd`;
	return $ret;
}

sub getip
{
	my $self = shift;
	my $ret = "";
	my $i;

	for $i (0..5)
	{
		$ret = `lxc-info $self->{'name'} -iH`;
		chomp($ret);
		last if($ret ne "");
		sleep 1;
	}
	return $ret;
}

sub _randname
{
	my $self = shift;

	my @charset = ('A'..'Z', 'a'..'z');
	join '', @charset[map {int rand @charset} (1..8)];
}

sub _netconfig
{
	my $self = shift;

	my %h;

	$h{'lxc.net.0.name'} = $self->{'netif'} if($self->{'netif'} ne "eth0");
	$h{'lxc.net.0.type'} = 'veth';
	$h{'lxc.net.0.flags'} = 'up';
	$h{'lxc.net.0.link'} = $self->{'bridge'};
	if($self->{'ipaddr'} ne 'dhcp')
	{
		$h{'lxc.net.0.ipv4'} = $self->{'ipaddr'};
		$h{'lxc.net.0.ipv4.gateway'} = $self->{'gw'};
	}
	$self->setconfig(%h);
	return(0);
}

sub _seterror
{
	my $self = shift;
	my $str = shift;

	$self->{'error'} = $str;
	return(0);
}

sub _iscreated
{
	my $self = shift;
	my $name = shift;

	$name = $self->{'name'} if(!defined($name));
    grep {/^$name$/} split("\n", `lxc-ls -1`);
}

sub _isrunning
{
	my $self = shift;
	my $name = shift;

	$name = $self->{'name'} if(!defined($name));
    grep {/^$name$/} split("\n", `lxc-ls -1 --running`);
}

sub _isstopped
{
	my $self = shift;
	my $name = shift;

	$name = $self->{'name'} if(!defined($name));
    grep {/^$name$/} split("\n", `lxc-ls -1 --stopped`);
}

sub _findlxcroot
{
	# TODO : Valid only for directory based containers.
	# Need to improve for LVM and others
	
	my $self = shift;
	my $name = shift;
	my $path;

	$name = $self->{'name'} if(!defined($name));
	$path = "/var/lib/lxc/$name";
	if(($self->{'backingstore'} eq "dir") || ($self->{'backingstore'} eq "overlayfs"))
	{
		return("$path/delta0") if( -d "$path/delta0");
		return("$path/rootfs") if(( ! -d "$path/delta0") && ( -d "$path/rootfs"));
		return('');
	}
	return('');
}

sub _findlxcconfig
{
	# TODO : Valid only for directory based containers.
	# Need to improve for LVM and others
	
	my $self = shift;
	my $name = shift;

	$name = $self->{'name'} if(!defined($name));
	return("/var/lib/lxc/$name/config") if(($self->{'backingstore'} eq "dir") || ($self->{'backingstore'} eq "overlayfs"));
	return('');
}
1;

