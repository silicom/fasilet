package ET2::Utils::Report;

# FaSilET²
# Copyright (C) 2019 Silicom
#
# Author : Cédric PELLERIN (cpellerin@silicom.fr)
# Latest version might be found at http://github.com/fasilet
#
# This file is part of FaSilET² project
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
use strict;
use warnings;
 
use Exporter qw(import);
	 
use Storable qw(dclone);
use Data::Structure::Util qw(unbless);
use JSON;

sub new
{
	my $class = shift;
	my (%params) = @_;

	my $self = {};

	bless($self, $class);
	$self->{'maintitle'} = $params{'title'} || $self->_randname();
	$self->{'maindescription'} = $params{'description'} || "";

	return $self;
}

sub setdescription
{
	my $self = shift;
	my $str = shift;

	$self->{'maindescription'} = $str;
}

sub getdescription
{
	my $self = shift;

	return $self->{'maindescription'};
}

sub addactdesc
{
	my $self = shift;
	my $act = shift;
	my $desc = shift;

	$self->{'actuators'}->{$act}->{'description'} = $desc;
}

sub getactdesc
{
	my $self = shift;
	my $act = shift;

	return $self->{'actuators'}->{$act}->{'description'};
}

sub addvalstatus
{
	my $self = shift;
	my $act = shift;
	my $val = shift;
	my $status = shift;

	$self->{'actuators'}->{$act}->{'status'}->{$val} = $status;
}

sub getvalstatus
{
	my $self = shift;
	my $act = shift;
	my $val = shift;

	return $self->{'actuators'}->{$act}->{'status'}->{$val};
}

sub addvalresults
{
	my $self = shift;
	my $act = shift;
	my $val = shift;
	my $res = shift;

	$self->{'actuators'}->{$act}->{'results'}->{$val} = {%$res};
}

sub getvalresults
{
	my $self = shift;
	my $act = shift;
	my $val = shift;

	return $self->{'actuators'}->{$act}->{'results'}->{$val};
}

sub addresult
{
	my $self = shift;
	my $act = shift;
	my $name = shift;
	my $res = shift;

	$self->{'actuators'}->{$act}->{'results'}->{$name}->{'main'} = $res;
}

sub getresult
{
	my $self = shift;
	my $act = shift;
	my $name = shift;

	return $self->{'actuators'}->{$act}->{'results'}->{$name}->{'main'};
}

sub addresultcomment
{
	my $self = shift;
	my $act = shift;
	my $name = shift;
	my $comment = shift;

	$self->{'actuators'}->{$act}->{'results'}->{$name}->{'comment'} = $comment;
}

sub getresultcomment
{
	my $self = shift;
	my $act = shift;
	my $name = shift;

	return $self->{'actuators'}->{$act}->{'results'}->{$name}->{'comment'};
}

sub addresultdesc
{
	my $self = shift;
	my $act = shift;
	my $name = shift;
	my $desc = shift;

	$self->{'actuators'}->{$act}->{'results'}->{$name}->{'description'} = $desc;
}

sub getresultdesc
{
	my $self = shift;
	my $act = shift;
	my $name = shift;

	return $self->{'actuators'}->{$act}->{'results'}->{$name}->{'description'};
}

sub exportjson
{
	my $self = shift;
	my $file = shift;
	my $data = shift;
	my $json;
	my $jsontext;
	my $fh;

	$json = JSON->new;
	$json->pretty(1);
	$json->indent(1);
	eval
	{
		$jsontext = $json->encode($data);
	};
	die("Error encoding report") if($@);
	open($fh, ">", $file) or die("Error creating $file cause $!");
	print $fh $jsontext;
	close($fh);
}

sub export
{
	my $self = shift;
	my $file = shift;
	my $copy;

	$copy = dclone($self);
	unbless($copy);
	$self->exportjson($file, $copy);
}

1;
