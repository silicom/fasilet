package ET2::Utils::Telnet;

# FaSilET²
# Copyright (C) 2019 Silicom
#
# Author : Cédric PELLERIN (cpellerin@silicom.fr)
# Latest version might be found at http://github.com/fasilet
#
# This file is part of FaSilET² project
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
use strict;
use warnings;
 
use Net::Telnet;

use Exporter qw(import);
	 
our @EXPORT_OK = qw(new connect close login command);

sub new
{
	my $class = shift;
	my $host = shift;
	my $port = shift;

	my $self = {};

	bless($self, $class);
	$self->{'host'} = $host;
	$self->{'port'} = $port;

	return $self;
}

sub connect
{
	my $self = shift;

	my $ret;
	my $conn;
	my $ok;

	$conn = new Net::Telnet(
		Timeout => 10,
	);
	
	$conn->errmode("die");
	$self->{'conn'} = $conn;
	$ok = $conn->open(
		Port => $self->{'port'},
		Host => $self->{'host'}
	);
	return $ok;
}

sub close
{
	my $self = shift;
	my $ok;

	my $conn = $self->{'conn'};
	$ok = $conn->close();
	return $ok;
}

sub login
{
	my $self = shift;
	my $login = shift;
	my $password = shift;
	my $ok;

	my $conn = $self->{'conn'};
	$ok = $conn->login($login, $password);
	return $ok;
}

sub command
{
	my $self = shift;
	my $cmdline = shift;
	my @output = ();

	my $conn = $self->{'conn'};
	@output = $conn->cmd($cmdline);
	chomp(@output);

	return @output;
}

1;
