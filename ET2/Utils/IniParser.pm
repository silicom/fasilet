package ET2::Utils::IniParser;

# FaSilET²
# Copyright (C) 2019 Silicom
#
# Author : Cédric PELLERIN (cpellerin@silicom.fr)
# Latest version might be found at http://github.com/fasilet
#
# This file is part of FaSilET² project
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
use strict;
use warnings;

use Config::Setting::Chunk;
use Exporter qw(import);
	 
sub new
{
	my $class = shift;
	my (%params) = @_;

	my $self = {};

	bless($self, $class);
	$self->{'chunk'} = Config::Setting::Chunk->new;

	return $self;
}

sub parse
{
	my $self = shift;
	my $file = shift;

	my $chunk = $self->{'chunk'};
	my $fh;
	my $section;
	my $key;
	my $value;

	return 1 if( ! -f $file);
    open ($fh, "<", $file) or return 1;
	while (<$fh>)
	{
		chomp;
		next if(/^\s*[#;]/);
		if (/^\s*\[(\w+)\].*/)
		{
			$section = $1;
			$chunk->add_section($section);
			$self->{'keysorder'}->{$section} ||= [];
		}
		if (/^\s*(\w+)\s*=\s*(.+)$/)
		{
			next unless $section;
			$key = $1;
			$value = $2 ;
			$chunk->set_item($section, $key, $value);
			push(@{$self->{'keysorder'}->{$section}}, $key);
		}
	}
    close($fh);

	return 0;
}

sub getsections
{
	my $self = shift;
	return($self->{'chunk'}->sections());
}

sub getitem
{
	my $self = shift;
	my $sect = shift;
	my $key = shift;

	return $self->{'chunk'}->get_item($sect, $key);
}

sub getkeys
{
	my $self = shift;
	my $section = shift;

	return(@{$self->{'keysorder'}->{$section}});
}
		


1;
