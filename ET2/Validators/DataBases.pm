package ET2::Validators::DataBases;

# FaSilET²
# Copyright (C) 2019 Silicom
#
# Author : Cédric PELLERIN (cpellerin@silicom.fr)
# Latest version might be found at http://github.com/fasilet
#
# This file is part of FaSilET² project
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

use strict;
use warnings;
 
use Encode qw(encode decode);
use DBI;

use ET2::Utils::SSH;
use ET2::Utils::Constants ':et2constants';

our $DEBUG = 1;

# Database type is coded in host parameter :
# sqlite://
# postgres://
# mysql:// (or mariadb://)
# oracle://
# etc.
#
# Only sqlite type need a SSH connection
#
sub new
{
	my $class = shift;
	my (%params) = @_;

	my $self = {};

	my $ssh;
	my $ret;
	my $dsn;
	my $dbtype;

	$class = ref($class) || $class;
	bless($self, $class);
	$self->{'host'} = $params{'host'} || "sqlite://127.0.0.1";
	$self->{'port'} = $params{'port'} || 0;
	$self->{'user'} = $params{'user'} || "";
	$self->{'password'} = $params{'password'} || "";
	$self->{'key'} = $params{'key'} || "";
	$self->{'database'} = $params{'database'} || "";
	$self->{'timestart'} = 0;
	$self->{'timestop'} = 3600;
	$DEBUG = $params{'debug'} || 0;

	if(defined($params{'timeout'}))
	{
		$self->settimeout($params{'timeout'});
	}

	if($self->{'host'} =~ /^(?<type>\w+):\/\/(?<ip>.+)$/)
	{
		$self->{'dbtype'} = $+{'type'};
		$self->{'host'} = $+{'ip'};
	}
	else
	{
		return(undef);
	}

	# We'll use ssh only for sqlite.
	# Other DB types are natively client-server architectured
	#
	if($self->{'dbtype'} =~ /^sqlite$/)
	{
		$self->{'port'} = 22 if($self->{'port'} == 0);
		# If we don't have key path, assume we are going to use login/password
		# else we use 'password' parameter as passphrase for key
		if($self->{'key'} eq "")
		{
			$ssh = ET2::Utils::SSH->new(
				host => $self->{'host'},
				port => $self->{'port'},
				user => $self->{'user'},
				password => $self->{'password'}
			);
		}
		else
		{
			$ssh = ET2::Utils::SSH->new(
				host => $self->{'host'},
				port => $self->{'port'},
				user => $self->{'user'},
				key => $self->{'key'},
				passphrase => $self->{'password'}
			);
		}
		$ret = $ssh->connect();
		# Error
		return undef if($ret->{'_error'} ne '0');

		$self->{'conn'} = $ssh;

	}
	elsif($self->{'dbtype'} =~ /^mysql|mariadb$/i)
	{
		$self->{'port'} = 3306 if($self->{'port'} == 0);
		$dsn = "DBI:mysql:database=$self->{'database'};host=$self->{'host'};port=$self->{'port'}";
		$self->{'conn'} = DBI->connect($dsn, $self->{'user'}, $self->{'password'});
	}
	elsif($self->{'dbtype'} =~ /^postgres$/i)
	{
		$self->{'port'} = 5432 if($self->{'port'} == 0);
		$dsn = "DBI:Pg:database=$self->{'database'};host=$self->{'host'};port=$self->{'port'}";
		$self->{'conn'} = DBI->connect($dsn, $self->{'user'}, $self->{'password'}, {AutoCommit => 0});
	}
	else
	{
		return(undef);
	}
	return $self;
}


sub setdebug
{
	my $self = shift;
	my $param = shift;

	$self->{'debug'} = $param if(defined($param));
	$DEBUG = $self->{'debug'} > 0 ? 1 : 0;
}

sub setname
{
	my $self = shift;
	my $param = shift;

	$self->{'name'} = $param if(defined($param));
}

sub settimeout
{
	my $self = shift;
	my $param = shift;

	return 1 if($param !~ /^\d+\.\.\d+$/);
	$param =~ /^(\d+)\.\.(\d+)$/;
	$self->{'timestart'} = $1;
	$self->{'timestop'} = $2;
	return 0;
}

sub setserver
{
	my $self = shift;
	my $param = shift;

	$self->{'host'} = $param if(defined($param));
}

sub setuser
{
	my $self = shift;
	my $param = shift;

	$self->{'user'} = $param if(defined($param));
}

sub setdatabase
{
	my $self = shift;
	my $param = shift;

	$self->{'database'} = $param if(defined($param));
}

sub setpassword
{
	my $self = shift;
	my $param = shift;

	$self->{'password'} = $param if(defined($param));
}

sub setregexok
{
	my $self = shift;
	my $param = shift;

	$self->{'regexok'} = $param if(defined($param));
}

sub setregexerror
{
	my $self = shift;
	my $param = shift;

	$self->{'regexerror'} = $param if(defined($param));
}

sub getresults
{
	my $self = shift;

	return $self->{'results'};
}

sub execute
{
	my $self = shift;
	my $sql = shift;
	my @data = ();
	my @lines;
	my $status = FALSE;
	my $sshconn;
	my $pid;
	my $results;
	my $flag = 0;
	my $sth;

	$results->{'params_OK'} = $self->{'regexok'};
	$results->{'params_ERROR'} = $self->{'regexerror'};
	$results->{'name'} = $self->{'name'} || "SGBD_Validator";
	$sshconn = $self->{'conn'}->{'conn'};

	print "Sleeping $self->{'timestart'} seconds\n" if($DEBUG);
	sleep($self->{'timestart'});

	print "Timeout in $self->{'timestop'} seconds\n" if($DEBUG);

	eval
	{
		local $SIG{ALRM} = sub 
		{
			$results->{'data'} = \@data;
			$results->{'status'} = TIMEOUT;
			$self->{'results'} = \$results;
			die("Alarm")
		};
		alarm($self->{'timestop'});

		while($flag == 0)
		{
			if($self->{'dbtype'} =~ /^sqlite$/)
			{
				@lines = $sshconn->capture("\"`which sqlite3` $self->{'database'} \'$sql\'\"");
				chomp(@lines);
				if(scalar(@lines) > 0)
				{
					push(@data, @lines);
					print "@lines -> ".scalar(@data)."\n" if($DEBUG);
				}
			}	

			if($self->{'dbtype'} =~ /^mysql|mariadb|postgres$/)
			{
				$sth = $self->{'conn'}->prepare($sql);
				$sth->execute();
				@lines = $sth->fetch_row_array();
				if(scalar(@lines) > 0)
				{
					push(@data, @lines);
					print "@lines -> ".scalar(@data)."\n" if($DEBUG);
				}
			}	

			


			if(defined($self->{'regexok'}) && ($line =~ /$self->{'regexok'}/))
			{
				alarm 0;
				$status = TRUE;
				$flag = 1;
				last;
			}
			


			if(defined($self->{'regexok'}) && ($line =~ /$self->{'regexok'}/))
			{
				alarm 0;
				$status = TRUE;
				$flag = 1;
				last;
			}
			if(defined($self->{'regexerror'}) && ($line =~ /$self->{'regexerror'}/))
			{
				alarm 0;
				$flag = 1;
				last;
			}
		}
		alarm 0;
		$results->{'status'} = $status;
		$results->{'data'} = \@data;
		$self->{'results'} = \$results;
	};
	alarm 0;
}

# Grep in file
sub grep
{
	my $self = shift;
	my @data = ();
	my $line;
	my $status = FALSE;
	my $sshconn;
	my $pid;
	my $results;
	my $flag = 0;

	$results->{'params_OK'} = $self->{'regexok'};
	$results->{'params_ERROR'} = $self->{'regexerror'};
	$results->{'name'} = $self->{'name'} || "SSHLog_Validator";
	$sshconn = $self->{'conn'}->{'conn'};

	print "Sleeping $self->{'timestart'} seconds\n" if($DEBUG);
	sleep($self->{'timestart'});

	print "Timeout in $self->{'timestop'} seconds\n" if($DEBUG);

	eval
	{
		local $SIG{ALRM} = sub 
		{
			$results->{'data'} = \@data;
			$results->{'status'} = TIMEOUT;
			$self->{'results'} = \$results;
			die("Alarm")
		};
		alarm($self->{'timestop'});

		while($flag == 0)
		{
			if(defined($self->{'regexok'}))
			{
				$line = "grep '".$self->{'regexok'}."' ".$self->{'logfile'}; 
				if($sshconn->test($line))
				{
					alarm 0;
					$status = TRUE;
					$flag = 1;
					last;
				}
			}
			if(defined($self->{'regexerror'}))
			{
				$line = "grep '".$self->{'regexerror'}."' ".$self->{'logfile'}; 
				if($sshconn->test($line))
				{
					alarm 0;
					$flag = 1;
					last;
				}
			}
			sleep(1);
		}
		alarm 0;
		$results->{'status'} = $status;
		$results->{'data'} = \@data;
		$self->{'results'} = \$results;
	};
	alarm 0;
}

1;
