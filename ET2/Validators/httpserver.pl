#!/usr/bin/perl

# FaSilET²
# Copyright (C) 2019 Silicom
#
# Author : Cédric PELLERIN (cpellerin@silicom.fr)
# Latest version might be found at http://github.com/fasilet
#
# This file is part of FaSilET² project
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# HTTP server with REST API
#
#
#
use strict;
use warnings;

use Net::HTTPServer;
use JSON;
use NetAddr::IP;
use IPC::System::Simple qw(capture capturex);

use Data::Dumper;

our $DEBUG = 1;
our $fhlog;
our $results = "";

my $logfile = "/tmp/httpserver.log";

my $server = new Net::HTTPServer();
$server->RegisterURL("/start",\&start);
$server->RegisterURL("/results",\&results);

open($fhlog, ">>", $logfile);
print $fhlog "**** Logs start ****\n";
if ( $server->Start() )
{
    $server->Process();
}
else
{
    print "Could not start the server.\n";
}

close($fhlog);

#***************************************************************************#
# Internal functions                                                        #
#***************************************************************************#


# Tool to print debug informations on screen and in system log
# IC :  text to print
# OC :  none
#
sub Debug
{
    my $txt = shift;

    return if($DEBUG == 0);
    print $fhlog "[DEBUG] - $txt\n";
    syslog("[DEBUG] - $txt", "info");
}

# Little function to help logging system messages
# IC :  message and level
# OC :  nothing
#
sub syslog
{
    my ($message, $level) = @_;
    my ($package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash);
    my $line1;

    ($package, $filename, $line) = caller;
    $line1 = $line;
    ($package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash) = caller(1);
    $message = "($subroutine - $line1 called from $line) : $message";
    chomp($message);
    $message =~ s/\v//g;
	print $fhlog "$level - $message\n";
}

# CmpVersions : compare two versions number
# IC : 2 version strings
# OC : -1 if v1 < v2, 0 if v1 = v2, 1 if v1 > v2
#
sub CmpVersions
{
    my ($v1, $v2) = @_;

    Debug((caller(0))[3]);
    $v1 =~ s/[^\.\d]//g;
    $v2 =~ s/[^\.\d]//g;

    my @vv1 = split(/\./, $v1);
    my @vv2 = split(/\./, $v2);

    my $ver1 = $vv1[0]*10+$vv1[1];
    my $ver2 = $vv2[0]*10+$vv2[1];

    return($ver1 <=> $ver2);
}

# GetTime : return current time and date nicely formatted
# IC :  none
# OC :  string
#
sub GetTime 
{

    Debug((caller(0))[3]);
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    my $nice_timestamp = sprintf ("%04d%02d%02d-%02d:%02d:%02d", $year+1900,$mon+1,$mday,$hour,$min,$sec);
    return($nice_timestamp);
}

# start : start validator
# IC :  POST JSON hash like 
#{
#  "name":"full path to validator",
#  "parameters": "[parameters list if needed]"
#}
# OC :  JSON hash like 
#       {"status":"ok|error"}
#
sub start
{
    my $req = shift;
    my $res = $req->Response();
    my $post;
    my $params;

	$results = "";
    $res->Header("Content-Type", "application/json");
    if($req->Env("REQUEST_METHOD") eq "POST")
    {
        $post = $req->{'REQUEST'};
        $post =~ s/^.*\cM\cJ\cM\cJ//sm;
		eval
		{
			$params = decode_json($post);
		};
		if($@)
        {
            $res->Print("{\"status\":\"error invalid JSON\"}");
            syslog("Internal error. Missing or invalid JSON parameter $_", "error");
            return($res);
        }

        if(!exists($params->{'name'}))
        {
            $res->Print("{\"status\":\"error missing 'name' parameter\"}");
            return($res);
        }
		$ENV{'ET2PARAMS'} = defined($params->{'parameters'}) ? $params->{'parameters'} : '';
		Debug("Executing /root/$params->{'name'}");
		$results = do("/root/$params->{'name'}");
		Debug("Returned from /root/$params->{'name'}");
		if(!defined($results))
		{
			$res->Print("{\"status\":\"error undefined results\"}");
		}
		else
		{
        	$res->Print("{\"status\":\"ok\"}");
		}
    }
    else
    {
        $res->Print("{\"status\":\"error GET unsupported\"}");
    }
	Debug("Sending back result $res");
    return($res);
}

sub results
{
    my $req = shift;
    my $res = $req->Response();
    my $ret;
    my $post;
    my $params;
    my $i;
    my $num;
	my $ti;

    Debug("RESULTS");

    Debug((caller(0))[3]);
    $res->Header("Content-Type", "application/json");
    if($req->Env("REQUEST_METHOD") eq "POST")
    {
        $post = $req->{'REQUEST'};
        $post =~ s/^.*\cM\cJ\cM\cJ//sm;
		eval
		{
			$params = decode_json($post);
		};
		if($@)
        {
            $res->Print("{\"status\":\"error : invalid JSON\"}");
            syslog("Internal error. Missing or invalid JSON parameter $_", "error");
            return($res);
        }

		if(ref($$results) eq 'HASH')
		{
			$ret = encode_json($$results);
        	$res->Print($ret);
		}
		else
		{
			$res->Print("{\"status\":\"error : empty results\"}");
		}
    }
    else
    {
        $res->Print("{\"status\":\"error : GET unsupported\"}");
    }
	return($res);
}
