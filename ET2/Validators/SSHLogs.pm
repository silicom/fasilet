package ET2::Validators::SSHLogs;

# FaSilET²
# Copyright (C) 2019 Silicom
#
# Author : Cédric PELLERIN (cpellerin@silicom.fr)
# Latest version might be found at http://github.com/fasilet
#
# This file is part of FaSilET² project
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

use strict;
use warnings;
 
use Encode qw(encode decode);

use ET2::Utils::SSH;
use ET2::Utils::Constants ':et2constants';

our $DEBUG = 1;

sub new
{
	my $class = shift;
	my (%params) = @_;

	my $self = {};

	my $ssh;
	my $ret;

	$class = ref($class) || $class;
	bless($self, $class);
	$self->{'host'} = $params{'host'} || "127.0.0.1";
	$self->{'port'} = $params{'port'} || 22;
	$self->{'user'} = $params{'user'} || "";
	$self->{'password'} = $params{'password'} || "";
	$self->{'key'} = $params{'key'} || "";
	$self->{'logfile'} = $params{'logfile'} || "/var/log/syslog";
	$self->{'timestart'} = 0;
	$self->{'timestop'} = 3600;
	$DEBUG = $params{'debug'} || 0;

	if(defined($params{'timeout'}))
	{
		$self->settimeout($params{'timeout'});
	}

	# If we don't have key path, assume we are going to use login/password
	# else we use 'password' parameter as passphrase for key
	if($self->{'key'} eq "")
	{
		$ssh = ET2::Utils::SSH->new(
			host => $self->{'host'},
			port => $self->{'port'},
			user => $self->{'user'},
			password => $self->{'password'}
		);
	}
	else
	{
		$ssh = ET2::Utils::SSH->new(
			host => $self->{'host'},
			port => $self->{'port'},
			user => $self->{'user'},
			key => $self->{'key'},
			passphrase => $self->{'password'}
		);
	}

	$ret = $ssh->connect();
	# Error
	return undef if($ret->{'_error'} ne '0');

	$self->{'conn'} = $ssh;

	return $self;
}


sub setdebug
{
	my $self = shift;
	my $param = shift;

	$self->{'debug'} = $param if(defined($param));
	$DEBUG = $self->{'debug'} > 0 ? 1 : 0;
}

sub setname
{
	my $self = shift;
	my $param = shift;

	$self->{'name'} = $param if(defined($param));
}

sub settimeout
{
	my $self = shift;
	my $param = shift;

	return 1 if($param !~ /^\d+\.\.\d+$/);
	$param =~ /^(\d+)\.\.(\d+)$/;
	$self->{'timestart'} = $1;
	$self->{'timestop'} = $2;
	return 0;
}

sub setserver
{
	my $self = shift;
	my $param = shift;

	$self->{'host'} = $param if(defined($param));
}

sub setuser
{
	my $self = shift;
	my $param = shift;

	$self->{'user'} = $param if(defined($param));
}

sub setlogfile
{
	my $self = shift;
	my $param = shift;

	$self->{'logfile'} = $param if(defined($param));
}

sub setpassword
{
	my $self = shift;
	my $param = shift;

	$self->{'password'} = $param if(defined($param));
}

sub setregexok
{
	my $self = shift;
	my $param = shift;

	$self->{'regexok'} = $param if(defined($param));
}

sub setregexerror
{
	my $self = shift;
	my $param = shift;

	$self->{'regexerror'} = $param if(defined($param));
}

sub getresults
{
	my $self = shift;

	return $self->{'results'};
}

# Tail -f on file
sub tail
{
	my $self = shift;
	my @data = ();
	my $line;
	my $status = FALSE;
	my $sshconn;
	my $pid;
	my $results;
	my $flag = 0;

	$results->{'params_OK'} = $self->{'regexok'};
	$results->{'params_ERROR'} = $self->{'regexerror'};
	$results->{'name'} = $self->{'name'} || "SSHLog_Validator";
	$sshconn = $self->{'conn'}->{'conn'};

	print "Sleeping $self->{'timestart'} seconds\n" if($DEBUG);
	sleep($self->{'timestart'});

	print "Timeout in $self->{'timestop'} seconds\n" if($DEBUG);

	eval
	{
		local $SIG{ALRM} = sub 
		{
			$results->{'data'} = \@data;
			$results->{'status'} = TIMEOUT;
			$self->{'results'} = \$results;
			die("Alarm")
		};
		alarm($self->{'timestop'});

		while($flag == 0)
		{
			# Need to read twice because of -n 1. Could use -n 0 but sed never see the line in this case so tail never quit
			$line = $sshconn->capture("tail -q -n 1 -F $self->{'logfile'}| sed '/.\\+/ q'");
			$line = $sshconn->capture("tail -q -n 1 $self->{'logfile'}");
			chomp($line);
			if($line ne '')
			{
				push(@data, $line);
				print "$line -> ".scalar(@data)."\n" if($DEBUG);
			}
			if(defined($self->{'regexok'}) && ($line =~ /$self->{'regexok'}/))
			{
				alarm 0;
				$status = TRUE;
				$flag = 1;
				last;
			}
			if(defined($self->{'regexerror'}) && ($line =~ /$self->{'regexerror'}/))
			{
				alarm 0;
				$flag = 1;
				last;
			}
		}
		alarm 0;
		$results->{'status'} = $status;
		$results->{'data'} = \@data;
		$self->{'results'} = \$results;
	};
	alarm 0;
}

# Grep in file
sub grep
{
	my $self = shift;
	my @data = ();
	my $line;
	my $status = FALSE;
	my $sshconn;
	my $pid;
	my $results;
	my $flag = 0;

	$results->{'params_OK'} = $self->{'regexok'};
	$results->{'params_ERROR'} = $self->{'regexerror'};
	$results->{'name'} = $self->{'name'} || "SSHLog_Validator";
	$sshconn = $self->{'conn'}->{'conn'};

	print "Sleeping $self->{'timestart'} seconds\n" if($DEBUG);
	sleep($self->{'timestart'});

	print "Timeout in $self->{'timestop'} seconds\n" if($DEBUG);

	eval
	{
		local $SIG{ALRM} = sub 
		{
			$results->{'data'} = \@data;
			$results->{'status'} = TIMEOUT;
			$self->{'results'} = \$results;
			die("Alarm")
		};
		alarm($self->{'timestop'});

		while($flag == 0)
		{
			if(defined($self->{'regexok'}))
			{
				$line = "grep '".$self->{'regexok'}."' ".$self->{'logfile'}; 
				if($sshconn->test($line))
				{
					alarm 0;
					$status = TRUE;
					$flag = 1;
					last;
				}
			}
			if(defined($self->{'regexerror'}))
			{
				$line = "grep '".$self->{'regexerror'}."' ".$self->{'logfile'}; 
				if($sshconn->test($line))
				{
					alarm 0;
					$flag = 1;
					last;
				}
			}
			sleep(1);
		}
		alarm 0;
		$results->{'status'} = $status;
		$results->{'data'} = \@data;
		$self->{'results'} = \$results;
	};
	alarm 0;
}

sub execute
{
	my $self = shift;
	my $cmd = shift;
	my @data = ();
	my $l;
	my $line;
	my $status = FALSE;
	my $sshconn;
	my $pid;
	my $results;
	my $flag = 0;

	return(undef) if(!defined($cmd) || ($cmd eq ''));
	$results->{'params_OK'} = $self->{'regexok'};
	$results->{'params_ERROR'} = $self->{'regexerror'};
	$results->{'name'} = $self->{'name'} || "SSHLog_Validator";
	$sshconn = $self->{'conn'}->{'conn'};

	print "Sleeping $self->{'timestart'} seconds\n" if($DEBUG);
	sleep($self->{'timestart'});

	print "Timeout in $self->{'timestop'} seconds\n" if($DEBUG);

	eval
	{
		local $SIG{ALRM} = sub 
		{
			$results->{'data'} = \@data;
			$results->{'status'} = TIMEOUT;
			$self->{'results'} = \$results;
			die("Alarm")
		};
		alarm($self->{'timestop'});

		while($flag == 0)
		{
			$cmd = "timeout $self->{'timestop'} ".$cmd;
			$line = $sshconn->capture($cmd);
			chomp($line);
			if($line ne '')
			{
				push(@data, $line);
				print "$line -> ".scalar(@data)."\n" if($DEBUG);
			}
			if(defined($self->{'regexok'}) && ($line =~ /$self->{'regexok'}/))
			{
				alarm 0;
				$status = TRUE;
				$flag = 1;
				last;
			}
			if(defined($self->{'regexerror'}) && ($line =~ /$self->{'regexerror'}/))
			{
				alarm 0;
				$flag = 1;
				last;
			}
		}
		alarm 0;
		$results->{'status'} = $status;
		$results->{'data'} = \@data;
		$self->{'results'} = \$results;
	};
	alarm 0;
}

1;
