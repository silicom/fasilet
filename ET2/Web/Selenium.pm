package ET2::Web::Selenium;

# FaSilET²
# Copyright (C) 2019 Silicom
#
# Author : Cédric PELLERIN (cpellerin@silicom.fr)
# Latest version might be found at http://github.com/fasilet
#
# This file is part of FaSilET² project
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

use strict;
use warnings;
 
use Selenium::Remote::Driver;
use Exporter qw(import);
	 
our @EXPORT_OK = qw(new regex timeout setname);
our $DEBUG = 0;

sub new
{
	my $class = shift;
	my (%params) = @_;

	my $self = {};

	my $profile;
	my $ret;
	my $port;
	my $pref;


	$class = ref($class) || $class;
	bless($self, $class);
	$self->{'host'} = $params{'host'} || "http://127.0.0.1";
	$self->{'port'} = $params{'port'} || $self->{'host'} =~ /^https/ ? 443 : 80;
	$self->{'browser'} = $params{'browser'} || "chrome";
	$self->{'encoding'} = $params{'encoding'} || "UTF-8";

	return $self;
}

sub start
{
	my $self = shift;
	my $driver;

	$self->{'driver'} = Selenium::Remote::Driver->new(
			'browser_name' => $self->{'browser'}
	);
}

sub setdebug
{
	my $self = shift;
	my $param = shift;

	$self->{'debug'} = $param if(defined($param));
	$DEBUG = $self->{'debug'} > 0 ? 1 : 0;
}

sub setname
{
	my $self = shift;
	my $param = shift;

	$self->{'name'} = $param if(defined($param));
}

sub goto
{
	my $self = shift;
	my $param = shift;

	$param = '/' if(!defined($param));
	$param = '/'.$param if($param !~ /^\//);
	$self->{'currentpage'} = $param;
	_getpage($self);
}


sub _getpage
{
	my $self = shift;

	my $url;
	my $ret;

	$url = $self->{'host'}.':'.$self->{'port'}.$self->{'currentpage'};
	$ret = $self->{'driver'}->get($url);
	return $ret;
}

sub _findelement
{
	my $self = shift;
	my (%params) = @_;
	my $sel = $self->{'driver'};

	my $elem = 1;

	if(exists($params{'name'}))
	{
		$elem = $sel->find_element_by_name($params{'name'});
		return 1 if($elem == 0);
	}
	if(exists($params{'id'}))
	{
		$elem = $sel->find_element_by_id($params{'id'});
		return 1 if($elem == 0);
	}
	if(exists($params{'class_name'}))
	{
		$elem = $sel->find_element_by_class_name($params{'class_name'});
		return 1 if($elem == 0);
	}
	if(exists($params{'xpath'}))
	{
		$elem = $sel->find_element_by_xpath($params{'xpath'});
		return 1 if($elem == 0);
	}
	if(exists($params{'css'}))
	{
		$elem = $sel->find_element_by_css($params{'css'});
		return 1 if($elem == 0);
	}

	return $elem;
}

sub _recodestring
{
	my $self = shift;
	my $data = shift;

	#return(decode($self->{'encoding'}, $data));
  	return(encode('utf-8', $data));
}

sub _str2ascii
{
	my $self = shift;
	my $data = shift;
	my $ret;

	$ret = encode('ascii', $data);
	$ret =~ s/\?+/\./g;

	return $ret;
}

sub winresize
{
	my $self = shift;
	my (%params) = @_;
	my $sel;

	my $x = $params{'x'} || 1024;
	my $y = $params{'y'} || 768;

	$sel = $self->{'driver'};
	$sel->set_window_size($x, $y);
}

sub write
{
	my $self = shift;
	my (%params) = @_;
	my $elem;
	my $sel;

	$sel = $self->{'driver'};
	$elem = $self->_findelement(%params);
	return 1 if($elem == 1);

	$elem->clear();
	$elem->send_keys($params{'value'});
	return 0;
}

sub clickbtn
{
	my $self = shift;
	my (%params) = @_;
	my $elem;
	my $sel;

	$sel = $self->{'driver'};
	$elem = $self->_findelement(%params);
	return 1 if($elem == 1);

	$elem->click();
}

sub read
{
	my $self = shift;
	my (%params) = @_;
	my $elem;
	my $sel;
	my $ret;

	$sel = $self->{'driver'};
	$elem = $self->_findelement(%params);
	return 1 if($elem == 1);

	$ret = $elem->get_value();
	$ret = $elem->get_text() if(!defined($ret));
	return $self->_recodestring($ret);
}

sub gettitle
{
	my $self = shift;
	my $sel;

	$sel = $self->{'driver'};
	return $self->_recodestring($sel->get_title());
}

sub geturl
{
	my $self = shift;
	my $sel;

	$sel = $self->{'driver'};
	return $sel->get_current_url();
}

sub stop
{
	my $self = shift;
	my $sel;

	$sel = $self->{'driver'};
	$sel->quit();
	return 0;
}

sub waitfortext
{
	my $self = shift;
	my (%prms) = @_;
	my $sel;
	my $txtcmp = 1;
	my $value;
	my $ret;
	my $str;
	my $txt = "";

	$sel = $self->{'driver'};
	$str = $self->_str2ascii($prms{'text'});

	eval
	{
		local $SIG{ALRM} = sub 
		{
			die($txtcmp, $txt, "\n")
		};
		alarm($prms{'timeout'});
		while($txtcmp == 1)
		{
			$txt = $self->read(%prms);
			$ret = $self->_str2ascii($txt);
			$txtcmp = 0 if($ret =~ /$str/);
			sleep(1);
		}
		alarm(0);
	};
	alarm(0);
	return ($txtcmp, $txt);
}

sub waitforpage
{
	my $self = shift;
	my (%prms) = @_;
	my $sel;
	my $pgcmp = 1;
	my $fcnt;
	my $value;
	my $ret;

	if(exists($prms{'url'}))
	{
		$fcnt = 'get_current_url';
		$value = $prms{'url'};
	}
	if(exists($prms{'title'}))
	{
		$fcnt = 'get_title';
		$value = $prms{'title'};
	}

	$sel = $self->{'driver'};
	eval
	{
		local $SIG{ALRM} = sub 
		{
			die($pgcmp)
		};
		alarm($prms{'timeout'});
		while($pgcmp == 1)
		{
			$ret = $sel->$fcnt;
			$pgcmp = 0 if($ret eq $value);
		}
		alarm(0);
	};
	alarm(0);
	return $pgcmp;
}

sub execute
{
	my $self = shift;
	my @data = ();
	my $line;
	my $status = "KO";
	my $sshconn;
	my $rout;
	my $pid;
	my $results;

	$results->{'params_OK'} = $self->{'regexok'};
	$results->{'params_ERROR'} = $self->{'regexerror'};
	$results->{'name'} = $self->{'name'} || "SSHLog_Validator";
	$sshconn = $self->{'conn'}->{'conn'};

	print "Sleeping $self->{'timestart'} seconds\n" if($DEBUG);
	sleep($self->{'timestart'});

	print "Timeout in $self->{'timestop'} seconds\n" if($DEBUG);
	eval
	{
		local $SIG{ALRM} = sub 
		{
			close $rout;
			$results->{'data'} = \@data;
			$results->{'status'} = "TIMEOUT";
			$self->{'results'} = \$results;
			die("Alarm")
		};
		alarm($self->{'timestop'});
		($rout, $pid) = $sshconn->pipe_out("tail -n 0 -f $self->{'logfile'}");

		while ($line = <$rout>)
		{
			chomp($line);
			push(@data, $line);
			print "$line\n" if($DEBUG);
			if(defined($self->{'regexok'}) && ($line =~ /$self->{'regexok'}/))
			{
				$status = "OK";
				last;
			}
			if(defined($self->{'regexerror'}) && ($line =~ /$self->{'regexerror'}/))
			{
				last;
			}
		}
		close $rout;
		$results->{'status'} = $status;
		$results->{'data'} = \@data;
		$self->{'results'} = \$results;
	}
}

1;
