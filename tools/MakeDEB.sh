#!/bin/bash

# Uncomment the next 2 lines for step by step run
#set -x
#trap read debug

#set -e

# Base working directory
#BASEDEV=${PWD}
BASE=/home/cedric/Developpement/Silicom/End2End
BASEDEV=${BASE}/ET2
# Where to put Debian packages
DEB_DEST_DIR=${BASEDEV}/Debs
# Where to create packages
DEB="${BASEDEV}/packages"

# Debien repository address if exists
#REPO="192.168.1.96"
REPO=""

# Automaticaly increment Debian packages version
# if commits since last GIT tag
INC_VERSION="n"

# First do some cleanup
if [ -d $DEB ]; then
	rm -rf $DEB
fi

mkdir $DEB
cd $DEB
rm -rf ${DEB_DEST_DIR}/*
mkdir -p ${DEB_DEST_DIR}


> ${DEB_DEST_DIR}/build.log

# Automaticaly increment version number in control files
function IncVersion
{
	# Exit if INV_VERSION disabled
	[ "_${INC_VERSION}" = "_y" ] || return 0

	pkg_src_dir=$1

	# Last GIT tag
	last_integration_tag=$(git -C ${BASEDEV} describe --tags --abbrev=0)

	# Get GIT history for current package
	git_history=$(git -C ${BASEDEV} shortlog -s ${last_integration_tag}..HEAD ${pkg_src_dir})

	if [ -n "${git_history}" ] ; then
		# Version++
		ver=`awk '$1~/^Version:/ {print $2}' $pkg_src_dir/DEBIAN/control`
		ver=`echo "$ver 0.01"|awk '{printf "%0.2f", $1 + $2}'`
		sed -i -e "s/^Version:.*/Version: $ver/" $pkg_src_dir/DEBIAN/control
	fi
}

# Log informations on screen and into build.log file
function log
{
	echo $1 | tee -a $DEB/build.log
}

# Here we go
# For every package :
# pkg <- package name
# dir <- list of directories to create
# Create directories
# Copy files into them
# Build package
#

######### ET2-base
#pkg='et2-base-perl'
#log "--> $pkg"
#mkdir $pkg
#dir='etc usr/bin usr/share/perl5'
#IncVersion "${BASEDEV}/Proxy"
#for i in $dir; do
#	mkdir -p ${pkg}/${i}
#done
#mkdir ${pkg}/DEBIAN
#cp $BASEDEV/DEBIAN/* $DEB/$pkg/DEBIAN
#cp $BASEDEV/ET2.pm $DEB/$pkg/usr/share/perl5
#dpkg-deb --build $pkg ${DEB_DEST_DIR} >> ${DEB_DEST_DIR}/build.log 2>&1

######### ET2 utilities and libraries
pkg='et2-utils-perl'
log "--> $pkg"
mkdir $pkg
dir='/usr/share/perl5/ET2/Utils'
for i in $dir; do
	mkdir -p ${pkg}/${i}
done
mkdir ${pkg}/DEBIAN
cp $BASEDEV/Utils/DEBIAN/* $DEB/$pkg/DEBIAN
cp $BASEDEV/Utils/*.pm $DEB/$pkg/$dir
dpkg-deb --build $pkg ${DEB_DEST_DIR} >> ${DEB_DEST_DIR}/build.log 2>&1

######### ET2 Actuators
pkg='et2-actuators-perl'
log "--> $pkg"
mkdir $pkg
dir='/usr/share/perl5/ET2/Actuators'
for i in $dir; do
	mkdir -p ${pkg}/${i}
done
mkdir ${pkg}/DEBIAN
cp $BASEDEV/Actuators/DEBIAN/* $DEB/$pkg/DEBIAN
cp $BASEDEV/Actuators/*.pm $DEB/$pkg/$dir
dpkg-deb --build $pkg ${DEB_DEST_DIR} >> ${DEB_DEST_DIR}/build.log 2>&1

######### ET2 Validators
pkg='et2-validators-perl'
log "--> $pkg"
mkdir $pkg
dir='/usr/share/perl5/ET2/Validators'
for i in $dir; do
	mkdir -p ${pkg}/${i}
done
mkdir ${pkg}/DEBIAN
cp $BASEDEV/Validators/DEBIAN/* $DEB/$pkg/DEBIAN
cp $BASEDEV/Validators/*.pm $DEB/$pkg/$dir
dpkg-deb --build $pkg ${DEB_DEST_DIR} >> ${DEB_DEST_DIR}/build.log 2>&1

######### ET2 Web
pkg='et2-web-perl'
log "--> $pkg"
mkdir $pkg
dir='/usr/share/perl5/ET2/Web'
for i in $dir; do
	mkdir -p ${pkg}/${i}
done
mkdir ${pkg}/DEBIAN
cp $BASEDEV/Web/DEBIAN/* $DEB/$pkg/DEBIAN
cp $BASEDEV/Web/*.pm $DEB/$pkg/$dir
dpkg-deb --build $pkg ${DEB_DEST_DIR} >> ${DEB_DEST_DIR}/build.log 2>&1

######### ET2 Metrology
pkg='et2-metrology-perl'
log "--> $pkg"
mkdir $pkg
dir='/usr/share/perl5/ET2/Metrology'
for i in $dir; do
	mkdir -p ${pkg}/${i}
done
mkdir ${pkg}/DEBIAN
cp $BASEDEV/Metrology/DEBIAN/* $DEB/$pkg/DEBIAN
cp $BASEDEV/Metrology/*.pm $DEB/$pkg/$dir
dpkg-deb --build $pkg ${DEB_DEST_DIR} >> ${DEB_DEST_DIR}/build.log 2>&1

# Copy .deb to repository
if [ "x$REPO" != "x" ]; then
	ping -q -W1 -c1 $REPO > /dev/null 2>&1
	if [ $? -eq 0 ]; then
		scp *.deb root@${REPO}:/var/www/html/SG4000
		ssh root@${REPO} 'cd /var/www/html && dpkg-scanpackages SG4000 | gzip > SG4000/Packages.gz'
	fi
fi

# Move deb files to Docker tree
rm -rf ${BASE}/Docker/Debs
mkdir ${BASE}/Docker/Debs
mv ${DEB_DEST_DIR}/*.deb ${BASE}/Docker/Debs

# Cleanup
rm -rf $DEB
rm -rf $DEB_DEST_DIR
